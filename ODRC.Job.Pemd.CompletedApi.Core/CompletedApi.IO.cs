﻿using Dapper;
using DapperExtensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;

namespace ODRC.Job.Pemd.CompletedApi.Core
{
    public static partial class CompletedApi
    {
        private static JobPemdCompletedApiResult SendRequest(
            ClaimDetail detail,
            dynamic payload,
            string completedApiUrlTemplate,
            string apiBaseUrl,
            StringLogger logger)
        {
            var resourceUrl = string.Format(completedApiUrlTemplate, detail.ResourceId);

            RestRequest request = new RestRequest(resourceUrl, Method.POST);
            request.AddHeader("Accept", "application/json");
            request.RequestFormat = DataFormat.Json;
            request.AddBody(payload);

            RestClient client = new RestClient(apiBaseUrl);
            var response = client.Execute<object>(request);

            JObject requestObject = JObject.FromObject(payload);
            var apiResult = new JobPemdCompletedApiResult();

            //apiResult.ClaimId = detail.ClaimID;
            //apiResult.Request = JsonConvert.SerializeObject(requestObject);
            //apiResult.ApiResult = ApiResult.Success;
            //apiResult.Response = JsonConvert.SerializeObject(new { Id = "123123", Message = "All good" });

            apiResult.ClaimId = detail.ClaimID;
            apiResult.RequestUrl = $"{apiBaseUrl}{resourceUrl}";
            try
            {
                requestObject["images"][0]["image"] = "[base64 encoded image content sent in actual payload - stripped in logs due to size]";
            }
            catch (Exception ex2)
            {
                logger.WriteErrorLog(ex2, "Error - stripping image from logs");
            }
            apiResult.Request = JsonConvert.SerializeObject(requestObject);
            apiResult.ApiResult = response.StatusCode == HttpStatusCode.OK
                    ? ApiResult.Success
                    : ApiResult.Failed;
            apiResult.ResponseStatusCode = response.StatusCode.ToString();

            try
            {
                apiResult.Response = JsonConvert.SerializeObject(JObject.FromObject(response.Data));
            }
            catch (Exception)
            {
                if (response.Data == null)
                    apiResult.Response = response.ErrorMessage;
                else
                    throw;
            }

            return apiResult;
        }

        public static void SaveBatch(SqlConnection connection, JobPemdBatch batch)
        {
            var sql = $@"
                IF EXISTS(SELECT COUNT(1) FROM [dbo].[JobPemdBatch] WHERE [Id] = @Id)
                BEGIN
	                UPDATE [dbo].[JobPemdBatch]
	                SET [Started] = @Started,
		                [Completed] = @Completed,
		                [Size] = @Size,
		                [DurationInSeconds] = @DurationInSeconds
	                WHERE [Id] = @Id
                END
                ELSE
                BEGIN
	                INSERT INTO [dbo].[JobPemdBatch]
		                ([Id]
		                ,[Started]
		                ,[Completed]
		                ,[Size]
		                ,[DurationInSeconds])
	                VALUES
		                (@Id
		                ,@Started
		                ,@Completed
		                ,@Size
		                ,@DurationInSeconds)
                END";

            connection.Execute(
                sql,
                new
                {
                    Id = batch.Id,
                    Started = batch.Started,
                    Completed = batch.Completed.HasValue ? batch.Completed : null,
                    Size = batch.Size,
                    DurationInSeconds = batch.DurationInSeconds
                });
        }

        private static void SendMail(MailMessage message)
        {
            message.IsBodyHtml = true;
            message.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(message.Body, null, MediaTypeNames.Text.Html));

            SmtpClient smtpClient = new SmtpClient(
                SmtpHost,
                Convert.ToInt32(SmtpPort));
            NetworkCredential credentials = new NetworkCredential(
                SmtpUserName,
                SmtpPassword);
            smtpClient.Credentials = credentials;
            smtpClient.EnableSsl = true;
            smtpClient.Send(message);
        }

        public static void RecordApiResults(
            SqlConnection connection,
            ICollection<JobPemdCompletedApiResult> results) =>
            results
                .ToList()
                .ForEach(x =>
                {
                    var insertSql = $@"INSERT INTO [dbo].[JobPemdCompletedApiResult]
                            ([JobPemdBatchId]
                            ,[ClaimId]
                            ,[ApiResult]
                            ,[RequestUrl]
                            ,[Request]
                            ,[Response]
                            ,[ResponseStatusCode]
                            ,[DurationInSeconds]
                            ,[RunDate])
                        VALUES
                            (@JobPemdBatchId
                            ,@ClaimId
                            ,@ApiResult
                            ,@RequestUrl
                            ,@Request
                            ,@Response
                            ,@ResponseStatusCode
                            ,@DurationInSeconds
                            ,@RunDate)";

                    connection.Execute(
                        insertSql,
                        new
                        {
                            JobPemdBatchId = x.JobPemdBatchId,
                            ClaimId = x.ClaimId,
                            ApiResult = (int)x.ApiResult,
                            RequestUrl = x.RequestUrl,
                            Request = x.Request,
                            Response = x.Response,
                            ResponseStatusCode = x.ResponseStatusCode,
                            DurationInSeconds = x.DurationInSeconds,
                            RunDate = x.RunDate
                        });
                });
        public static ICollection<ClaimDetail> GetClaimDetails(
            DbTransaction transaction,
            IList<Guid> claimIds)
        {
            var claimDetails = transaction.Connection.Query<ClaimDetail>(@"
				select c.ClaimID,
	                c.[Status] 'Status',
	                plp.UrlPath,
	                c.AlternateClaimIDValue,
	                cu.FirstName,
	                cu.LastName,
	                cu.Email,
	                cu.Address1,
	                cu.Address2,
	                cu.City,
	                cu.[State],
	                cu.ZipCode,
	                cu.MailingAddress1,
	                cu.MailingAddress2,
	                cu.MailingCity,
	                cu.MailingState,
	                cu.MailingZipCode,
	                cu.Phone,
	                cu.MobilePhone,
	                ISNULL(c.ModifiedBy, c.CreatedBy),
	                c.CreatedDate,
	                c.ModifiedDate,
	                r.DollarValue 'RewardDollarValue'
                from Claims c
                join ProgramLandingPages plp
	                on c.ProgramID = plp.ProgramID
                join ClaimUsers cu
	                on cu.ClaimUserId = c.ClaimUserId
                join ClaimRewards cr
	                on cr.ClaimID = c.ClaimID
                join Rewards r
	                on r.RewardID = cr.RewardID
                where c.ClaimID IN @ClaimIDs
				",
                new
                {
                    ClaimIDs = claimIds.ToArray()
                },
                transaction)
                .ToList();

            claimDetails.ForEach(x =>
            {
                x.FieldValues = transaction.Connection.Query<FieldValue>(@"
                SELECT 
	                FieldValue 'Value', 
	                Question 'Field'
                FROM ClaimFieldValues
                WHERE ClaimID = @ClaimID",
                new
                {
                    ClaimID = x.ClaimID
                },
                transaction)
                .ToList();
            });

            return claimDetails;
        }

        public static ICollection<JobPemdCompletedApiTrigger> GetCompletedApiTriggers(DbConnection connection) =>
            connection.Query<JobPemdCompletedApiTrigger>(@"
                 SELECT  [Id]
                      ,j.[ClaimId]
                      ,j.[ApiFetchStatus]
                      ,j.[Created]
                      ,j.[Updated]
                FROM [JobPemdCompletedApiTrigger] j
				JOIN [Claims] c
					ON c.ClaimID = j.ClaimID
                WHERE [ApiFetchStatus] = 0
				AND c.ProgramID IN @ProgramIDs",
                new
                {
                    ProgramIDs = ProgramIDs
                })
                .ToList();

        public static void SetTriggersStatus(
            ApiFetchStatus status,
            ICollection<JobPemdCompletedApiTrigger> triggers,
            DbTransaction transaction) =>
            triggers
                .ToList()
                .ForEach(x => transaction.Connection.Execute(@"
                    UPDATE [dbo].[JobPemdCompletedApiTrigger]
                        SET ApiFetchStatus = @Status,
                            Updated = @Updated
                    WHERE Id = @Id",
                    new
                    {
                        Id = x.Id,
                        Status = (int)status,
                        Updated = DateTime.Now
                    },
                    transaction));

    }
}
