﻿
USE [ODRCDB]
GO

/****** Object:  Table [dbo].[JobPemdCompletedApiResult]    Script Date: 10/25/2017 2:59:32 AM ******/
DROP TABLE [dbo].[JobPemdCompletedApiResult]
GO

/****** Object:  Table [dbo].[JobPemdCompletedApiResult]    Script Date: 10/25/2017 2:59:32 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[JobPemdCompletedApiResult](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[JobPemdBatchId] [uniqueidentifier] NOT NULL,
	[ClaimId] [uniqueidentifier] NOT NULL,
	[ApiResult] [int] NOT NULL,
	[RequestUrl] [varchar](150) NOT NULL,
	[Request] [text] NULL,
	[Response] [text] NULL,
	[ResponseStatusCode] [varchar](150) NULL,
	[DurationInSeconds] [bigint] NOT NULL,
	[RunDate] [datetime] not null
 CONSTRAINT [PK_JobPemdCompletedApiResult] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO



USE [ODRCDB]
GO

/****** Object:  Table [dbo].[JobPemdBatch]    Script Date: 10/25/2017 2:58:36 AM ******/
DROP TABLE [dbo].[JobPemdBatch]
GO

/****** Object:  Table [dbo].[JobPemdBatch]    Script Date: 10/25/2017 2:58:36 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[JobPemdBatch](
	[Id] [uniqueidentifier] NOT NULL,
	[Started] [datetime] NOT NULL,
	[Completed] [datetime] NULL,
	[Size] [bigint] NOT NULL,
	[DurationInSeconds] [bigint] NOT NULL
) ON [PRIMARY]

GO

USE [ODRCDB]
GO

/****** Object:  Table [dbo].[JobPemdCompletedApiTrigger]    Script Date: 10/25/2017 3:00:15 AM ******/
DROP TABLE [dbo].[JobPemdCompletedApiTrigger]
GO

/****** Object:  Table [dbo].[JobPemdCompletedApiTrigger]    Script Date: 10/25/2017 3:00:15 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[JobPemdCompletedApiTrigger](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ClaimId] [uniqueidentifier] NOT NULL,
	[ApiFetchStatus] [int] NOT NULL,
	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
 CONSTRAINT [PK_JobPemdCompletedApiTrigger] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



