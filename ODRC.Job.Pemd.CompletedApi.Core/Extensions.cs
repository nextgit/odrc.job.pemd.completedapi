﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;

namespace ODRC.Job.Pemd.CompletedApi.Core
{
    public static class Extensions
    {
        public static string GenerateCode()
        {
            var returnVal = string.Empty;

            // Taken from the Rebate Center -  as is...
            string Codes = Guid.NewGuid().ToString().Replace("-", "");
            Codes = Codes.Substring(0, 4) + "-" + Codes.Substring(14, 6) + "-" + Codes.Substring(Codes.Length - 4);

            return Codes;
        }

        public static string GetShortClaimID(this Guid id)
        {
            return "R" + id.ToString().Substring(0, 8);
        }

		public static string GetSpecifiedLengthString(this Guid value, int length) =>
			value.ToString().Substring(0, length);

		public static string GetSpecifiedLengthString(this string value, int length) => 
            string.IsNullOrEmpty(value) || value.Length < length
            ? string.Empty
            : value.Substring(0, length);

        public static string ToJson(this ExpandoObject expando)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            StringBuilder json = new StringBuilder();
            List<string> keyPairs = new List<string>();
            IDictionary<string, object> dictionary = expando as IDictionary<string, object>;
            json.Append("{");

            foreach (KeyValuePair<string, object> pair in dictionary)
            {
                if (pair.Value is ExpandoObject)
                {
                    keyPairs.Add(String.Format(@"""{0}"": {1}", pair.Key, (pair.Value as ExpandoObject).ToJson()));
                }
                else
                {
                    keyPairs.Add(String.Format(@"""{0}"": {1}", pair.Key, serializer.Serialize(pair.Value)));
                }
            }

            json.Append(String.Join(",", keyPairs.ToArray()));
            json.Append("}");

            return json.ToString();
        }

        public static string GetValueFromFieldDetails(
            this IEnumerable<FieldValue> details,
            string key,
            LookupMode mode = LookupMode.StartsWith,
            string defaultValueIfNull = null,
            string formatting = "")
        {
            Func<string, string, bool> comparer = null;
            switch (mode)
            {
                case LookupMode.StartsWith:
                    comparer = (x, y) => x.StartsWith(y, StringComparison.CurrentCultureIgnoreCase);
                    break;
                case LookupMode.Contains:
                    comparer = (x, y) => x.Contains(y);
                    break;
                case LookupMode.ExactMatch:
                    comparer = (x, y) => x.Equals(y, StringComparison.CurrentCultureIgnoreCase);
                    break;
                case LookupMode.ExactMatchCaseSensitive:
                    comparer = (x, y) => x.Equals(y);
                    break;
            }

            var result = details.Where(x => comparer(x.Field, key)).Select(x => x.Value).FirstOrDefault();
            
            return result == null
                ? defaultValueIfNull
                : result;
        }
    }
}
