﻿using Dapper;
using DapperExtensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;

namespace ODRC.Job.Pemd.CompletedApi.Core
{
    public static partial class CompletedApi
    {
        public static ICollection<JobPemdCompletedApiResult> PostCompletedApiRequests(
            Guid batchId,
            ICollection<ClaimDetail> details,
            StringLogger logger)
        {
            return details
                .ToList()
                .Select(x =>
                {
                    Stopwatch stopwatch = new Stopwatch();
                    stopwatch.Start();

                    dynamic payload = null;
                    JobPemdCompletedApiResult result = null;

                    // Step 2 - Send the Request and return the result
                    var completedApiUrl = CompletedApi.ShouldSendToTestOnly(x.AccountNumber)
                        ? CompletedApi.TestCompletedApiUrlTemplate
                        : CompletedApi.CompletedApiUrlTemplate;

                    var apiBaseUrl = CompletedApi.ShouldSendToTestOnly(x.AccountNumber)
                        ? CompletedApi.TestApiBaseUrl
                        : CompletedApi.ApiBaseUrl;

                    try
                    {
                        // Step 1 - Parse the ClaimDetaisls into the JSON payload within the request
                        payload = ParseClaimDetailsToJson(x, logger);

                        result = SendRequest(
                            x,
                            payload,
                            completedApiUrl,
                            apiBaseUrl,
                            logger);
                    }
                    catch (TimeoutException ex)
                    {
                        try
                        {
                            payload["images"][0]["image"] = "[base64 encoded image content sent in actual payload - stripped in logs due to size]";
                        }
                        catch (Exception ex2)
                        {
                            logger.WriteErrorLog(ex2, "Error - stripping image from logs");
                        }
                        result = new JobPemdCompletedApiResult
                        {
                            ApiResult = ApiResult.Abandoned,
                            RequestUrl = $"{apiBaseUrl}{completedApiUrl}",
                            Request = payload == null
                                ? null
                                : JsonConvert.SerializeObject(JObject.FromObject(payload)),
                            Response = JsonConvert.SerializeObject(JObject.FromObject(ex))
                        };
                    }
                    catch (Exception ex1)
                    {
                        try
                        {
                            payload["images"][0]["image"] = "[base64 encoded image content sent in actual payload - stripped in logs due to size]";
                        }
                        catch (Exception ex2)
                        {
                            logger.WriteErrorLog(ex2, "Error - stripping image from logs");
                        }
                        result = new JobPemdCompletedApiResult
                        {
                            ApiResult = ApiResult.Error,
                            RequestUrl = $"{apiBaseUrl}{completedApiUrl}",
                            Request = payload == null
                                ? null
                                : JsonConvert.SerializeObject(JObject.FromObject(payload)),
                            Response = JsonConvert.SerializeObject(JObject.FromObject(ex1))
                        };
                    }

                    result.ClaimId = x.ClaimID;
                    result.JobPemdBatchId = batchId;
                    result.RunDate = DateTime.Now;
                    stopwatch.Stop();
                    result.DurationInSeconds = stopwatch.ElapsedMilliseconds / 1000;
                    return result;
                })
                .ToList();
        }

        private static bool ShouldSendToTestOnly(string accountNumber) =>
            SendToTestOnly || TestAccountNumbers.Where(x => x.Equals(accountNumber)).Any();

        private static dynamic ParseClaimDetailsToJson(ClaimDetail detail, StringLogger logger)
        {
            switch (detail.UrlPath.ToLower())
            {
                case "pemdheaterw":
                case "pemdcfllightingw":
                case "pemdpoolpumpw":
                case "pemdfrigwasherdryerw":
                case "pemdroomacw":
                    {
                        return MapApplianceToDynamic(detail, logger);
                    }
            }
            return null;
        }

        public static void SendApiCompletionNotification(
            JobPemdBatch batch,
            ICollection<JobPemdCompletedApiResult> results)
        {
            StringBuilder summary = new StringBuilder();

            summary.AppendLine("ODRC Job - Completed API Summary");

            summary.AppendLine($"Total # of Records: {batch.Size} <br/>");
            summary.AppendLine($"Succeeded count: {results.Where(x => x.ApiResult == ApiResult.Success).Count()} <br/>");
            summary.AppendLine($"Failed count: {results.Where(x => x.ApiResult == ApiResult.Failed).Count()} <br/>");
            summary.AppendLine($"Error count: {results.Where(x => x.ApiResult == ApiResult.Error).Count()} <br/>");
            summary.AppendLine($"Abandoned count: {results.Where(x => x.ApiResult == ApiResult.Abandoned).Count()} <br/>");
            summary.AppendLine($"Duration: {((batch.Completed ?? DateTime.Now) - batch.Started).TotalSeconds} seconds <br/>");
            summary.AppendLine($"Started: {batch.Started} <br/>");
            summary.AppendLine($"Completed: {batch.Completed} <br/>");
            summary.AppendLine($"Batch Reference ID: {batch.Id} <br/>");

            summary.AppendLine("<table>");
            summary.AppendLine("<tr><th>ClaimId</th><th>Result</th><th>Duration</th><th>Url</th><th>ResponseStatus</th><th>Response</th></tr>");
            results
                .ToList()
                .ForEach(result =>
                {
                    summary.AppendLine($"<tr><td>{result.ClaimId}</td><td>{result.ApiResult.ToString()}</td><td>{result.DurationInSeconds}</td><td>{result.RequestUrl}</td><td>{result.ResponseStatusCode}</td><td>{result.Response}</td></tr>");
                });
            summary.AppendLine("</table>");

            string from = "notification@nxtpay.com";
            string subject = $"[ODRC-Job-Pemd-CompletedApi][{Environment.MachineName}] Completed Api Summary {DateTime.Now.ToString("yyyyMMdd")} {DateTime.Now.ToString("HHmmssfff")}";

            var message = new MailMessage();
            ToAddressList
                .Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                .ToList()
                .ForEach(x => message.To.Add(new MailAddress(x)));
            message.From = new MailAddress(from);
            message.Body = summary.ToString();
            message.IsBodyHtml = true;
            message.Subject = subject;
            SendMail(message);
        }

        private static dynamic MapApplianceToDynamic(
            ClaimDetail detail,
            StringLogger logger)
        {
            dynamic req = new ExpandoObject();

            // Demographics
            req.sourceID = detail.AlternateClaimIDValue;
            req.account = detail.FieldValues.GetValueFromFieldDetails("Account #");
            req.accountProvider = AccountProvider;
            req.name = string.IsNullOrEmpty(detail.LastName)
                ? detail.FirstName
                : $"{detail.FirstName} {detail.LastName}"; // detail.CustomerName;

            req.first = detail.FirstName;
            req.last = detail.LastName;
            req.email = detail.Email;
            req.serviceAddress = detail.Address1;
            req.serviceCity = detail.City;
            req.serviceState = detail.State;
            req.serviceZip = detail.ZipCode;
            req.mailingAddress = detail.MailingAddress1;
            req.mailingCity = detail.MailingCity;
            req.mailingState = detail.MailingState;
            req.mailingZip = detail.MailingZipCode;
            req.phone1 = detail.Phone;
            req.phone2 = detail.MobilePhone;
            req.userID = UserID;
            req.applicationID = detail.ClaimID.GetSpecifiedLengthString(10);

            //customerAttributes: List<attribute> // custom customer data points
            var customerAttributesList = new List<dynamic>();

            Action<string, dynamic> addToCustomerAttributesListIfNotNull = (name, value) =>
             {
                 if (value != null)
                 {
                     dynamic data = new ExpandoObject();
                     data.name = name;
                     data.value = value;
                     customerAttributesList.Add(data);
                 }
             };

            string purchaseDate = detail.FieldValues.GetValueFromFieldDetails("Purchase date") == null
                ? null
                : Convert.ToDateTime(detail.FieldValues.GetValueFromFieldDetails("Purchase date")).ToString(DateFormat);

            addToCustomerAttributesListIfNotNull("DATE OF SERVICE/PURCHASE", purchaseDate);

            string dateOfService = detail.FieldValues.GetValueFromFieldDetails("How did you hear");
            addToCustomerAttributesListIfNotNull("HOW DID YOU HEAR", dateOfService);

            string naturalGas = detail.FieldValues.GetValueFromFieldDetails("Is natural gas");
            int? hasNaturalGas = naturalGas == null
                ? (int?)null
                : naturalGas.Equals("Yes") ? 1 : 0;
            addToCustomerAttributesListIfNotNull("NATURAL GAS AVAILABLE", hasNaturalGas);

            string homeElectric = detail.FieldValues.GetValueFromFieldDetails("Does your home have an electric");
            int? hasHomeElectric = homeElectric == null
                ? (int?)null
                : homeElectric.Equals("Yes") ? 1 : 0;
            addToCustomerAttributesListIfNotNull("ELECTRIC WATER HEATER", hasHomeElectric);

            string receiptFileName = detail.FieldValues.GetValueFromFieldDetails("Receipt");
            int? hasReceipt = receiptFileName == null
                ? (int?)null
                : string.IsNullOrEmpty(receiptFileName) ? 0 : 1;
            addToCustomerAttributesListIfNotNull("RECEIPT", hasReceipt);

            string submissionType = detail.FieldValues.GetValueFromFieldDetails("Submission type");
            submissionType = submissionType ?? "";
            addToCustomerAttributesListIfNotNull("SUBMISSION TYPE", submissionType);

            // Add optIn
            customerAttributesList.Add(new { name = "OPT IN", value = "1" });

            customerAttributesList.Add(new { name = "DATE APPLICATION RECEIVED", value = detail.CreatedDate.ToString(DateFormat) });

            var postmarkDate = detail.FieldValues.GetValueFromFieldDetails("Postmark Date");
            postmarkDate = !string.IsNullOrEmpty(postmarkDate) ? Convert.ToDateTime(postmarkDate).ToString(DateFormat) : DateTime.Now.ToString(DateFormat);

            customerAttributesList.Add(new { name = "POSTMARK DATE", value = postmarkDate });

            req.customerAttributes = customerAttributesList;

            //measures: List<measure>     // rebate measures
            var measures = new List<dynamic>();
            dynamic measure = new ExpandoObject();
            measure.quantity = 1;
            measure.measureID = detail.AlternateClaimIDValue;
            var measureAttributes = new List<dynamic>();

            Action<string, string, bool> addToMeasureAttributesListIfNotNull = (name, value, emptyIfNull) =>
            {
                if (value != null || emptyIfNull)
                {
                    dynamic data = new ExpandoObject();
                    data.name = name;
                    data.value = value == null
                        ? ""
                        : value;
                    measureAttributes.Add(data);
                }
            };

            var modelNumber = string.Empty;
            var programUrl = detail.UrlPath.ToLower();

            modelNumber = detail.FieldValues.GetValueFromFieldDetails("Model #");

            addToMeasureAttributesListIfNotNull("MODEL NUMBER", modelNumber, false);

            var quantity = detail.FieldValues.GetValueFromFieldDetails("Quantity");

            if (quantity != null)
            {
                measure.quantity = Convert.ToInt16(quantity);
                addToMeasureAttributesListIfNotNull("Quantity", quantity, false);
            }

            var manufacturer = detail.FieldValues.GetValueFromFieldDetails("Manufacturer") ??
                            detail.FieldValues.GetValueFromFieldDetails("Product Brand");
            addToMeasureAttributesListIfNotNull("MANUFACTURER/BRAND", manufacturer, false);

            var storeName = detail.FieldValues.GetValueFromFieldDetails("Store / Contractor name") ??
                            detail.FieldValues.GetValueFromFieldDetails("Store name");
            addToMeasureAttributesListIfNotNull("STORE NAME", storeName, false);

            var storeCity = detail.FieldValues.GetValueFromFieldDetails("Store / Contractor location") ??
                detail.FieldValues.GetValueFromFieldDetails("Store city");
            addToMeasureAttributesListIfNotNull("STORE LOCATION", storeCity, false);

            addToMeasureAttributesListIfNotNull("PURCHASE DATE", purchaseDate, false);

            addToMeasureAttributesListIfNotNull("TOTAL PURCHASE PRICE", detail.FieldValues.GetValueFromFieldDetails("Purchase price"), false);

            addToMeasureAttributesListIfNotNull("AGE OF EXISTING", detail.FieldValues.GetValueFromFieldDetails("Age of replaced"), false);

            addToMeasureAttributesListIfNotNull("EXISTING MODEL NO", detail.FieldValues.GetValueFromFieldDetails("What model was replaced"), false);

            var fieldValue = detail.FieldValues.GetValueFromFieldDetails("Was your pool calibrated");
            if (fieldValue != null)
            {
                fieldValue = fieldValue == "Yes"
                ? "1"
                : "0";
                addToMeasureAttributesListIfNotNull("PP CALLIBRATED", fieldValue, false);
            }

            addToMeasureAttributesListIfNotNull("SERIAL NUMBER", detail.FieldValues.GetValueFromFieldDetails("Serial"), false);

            var installType = detail.FieldValues.GetValueFromFieldDetails("How was the pool pump");
            if (installType != null)
            {
                installType = installType.StartsWith("Self Installed", StringComparison.InvariantCultureIgnoreCase)
                    ? "1"
                    : "0";

                addToMeasureAttributesListIfNotNull("INSTALLED TYPE", installType, false);
            }

            var measureCode = detail.FieldValues.GetValueFromFieldDetails("Measure Code");
            measure.productType = measureCode ?? "";

            addToMeasureAttributesListIfNotNull("ENERGY FACTOR", detail.FieldValues.GetValueFromFieldDetails("Energy EF"), false);

            addToMeasureAttributesListIfNotNull("MEF", detail.FieldValues.GetValueFromFieldDetails("Integrated Modified Energy Factor"), false);

            addToMeasureAttributesListIfNotNull("ENERGYSTAR MODEL", detail.FieldValues.GetValueFromFieldDetails("Energy Star Matched Model Number"), false);

            addToMeasureAttributesListIfNotNull("CATEGORY (CONFIGURATION)", detail.FieldValues.GetValueFromFieldDetails("Category"), false);

            addToMeasureAttributesListIfNotNull("PERCENT BETTER THAN FED STANDARDS", detail.FieldValues.GetValueFromFieldDetails("Percentage Better Than Fed Standards"), false);

            // Add Other measureAttributes
            addToMeasureAttributesListIfNotNull("EQUIPMENT ON RECEIPT", "1", true);
            addToMeasureAttributesListIfNotNull("RECEIPT IS LEGIBLE", "1", true);
            addToMeasureAttributesListIfNotNull("PAID IN FULL", "1", true);

            // CEE ES data
            measure.measureAttributes = measureAttributes;
            measures.Add(measure);
            req.measures = measures;

            // rebate payments
            var rebates = new List<dynamic>();
            dynamic rebate = new ExpandoObject();
            rebate.accountID = AccountID; //As per Jill, this should be set to a unique identifier instead // claim.ClaimID.GetSpecifiedLengthString(10);
            rebate.payeeName = string.IsNullOrEmpty(detail.LastName)
                ? detail.FirstName
                : $"{detail.FirstName} {detail.LastName}";
            rebate.payeeAddress = detail.MailingAddress1;
            rebate.payeeCity = detail.MailingCity;
            rebate.payeeState = detail.MailingState;
            rebate.payeeZip = detail.MailingZipCode;
            rebate.paymentType = RewardTypes.Check.ToString();
            rebate.status = GetHwStatusByClaimStatus(detail.Status).ToString();

            var paymentNumber = detail.FieldValues.GetValueFromFieldDetails("Payment Number") ?? "";
            var dateEntered = detail.FieldValues.GetValueFromFieldDetails("Check Date");
            dateEntered = string.IsNullOrEmpty(dateEntered)
                ? ""
                : Convert.ToDateTime(dateEntered).ToString(DateFormat);

            rebate.paymentNumber = paymentNumber;
            rebate.dateEntered = dateEntered;

            rebate.rebateAmount = detail.RewardDollarValue;
            rebate.statusDate = (detail.ModifiedDate ?? detail.CreatedDate).ToString(DateFormat);
            rebate.approved = 1; // 1 = yes, 0 = no
            rebate.commited = 1; // 1 = yes, 0 = no
            rebate.checkAmount = detail.RewardDollarValue;

            // sc 201612 null out the rebate values if they status is decline
            if (detail.Status != ClaimStatus.Approved)
            {
                //As per Jill, this should be set to a unique identifier instead // claim.ClaimID.GetSpecifiedLengthString(10);
                rebate.payeeName = null;
                rebate.payeeAddress = null;
                rebate.payeeCity = null;
                rebate.payeeState = null;
                rebate.payeeZip = null;
                rebate.paymentType = null;
                rebate.status = null;
                rebate.paymentNumber = null;
                rebate.dateEntered = null;
                rebate.rebateAmount = null;
                rebate.statusDate = null;
                rebate.approved = null;
                rebate.commited = null;
                rebate.checkAmount = null;
            }

            rebates.Add(rebate);
            req.rebates = rebates;

            //interactions: List<interaction>     // interactions with the customer
            var interactions = new List<dynamic>();
            dynamic interaction = new ExpandoObject();
            //interaction.date = (claim.ModifiedDate ?? claim.CreatedDate).ToString("s");
            //interaction.type = "Website";
            //interaction.notes = GetHwStatusTextByClaimStatus(claim.Status);
            //interaction.reference = "Next";
            //interactions.Add(interaction);
            //req.interactions = interactions;

            // applicationStatus : {}
            dynamic applicationStatus = new ExpandoObject();
            applicationStatus.date = (detail.ModifiedDate ?? detail.CreatedDate).ToString(DateFormat);
            applicationStatus.description = GetHwStatusTextByClaimStatus(detail.Status);
            applicationStatus.reason = GetHwReasonTextByClaimStatus(detail.Status);

            req.applicationStatus = applicationStatus;

            // images : []
            if (!string.IsNullOrEmpty(receiptFileName))
            {
                var images = new List<dynamic>();
                dynamic image = new ExpandoObject();
                image.fileName = receiptFileName;
                image.image = GetImageByteAsString(
                    BaseFilePath,
                    receiptFileName,
                    logger);
                images.Add(image);

                req.images = images;
            }

            logger.WriteInfoLog("Completed mapping Appliance Claim.");
            return req;
        }

        private static string GetImageByteAsString(
            string basePath,
            string fileName,
            StringLogger logger)
        {
            var result = string.Empty;

            try
            {
                using (var memoryStream = File.OpenRead($"{basePath}\\{fileName}"))
                {
                    byte[] imageBytes = new byte[memoryStream.Length];

                    memoryStream.Read(imageBytes, 0, (int)memoryStream.Length);

                    result = Convert.ToBase64String(imageBytes);
                }
            }
            catch (Exception e)
            {
                logger.WriteErrorLog(e, "There where errors getting the image bytes as string - {0}", fileName);
                throw;
            }

            return result;
        }

        private static string GetHwReasonTextByClaimStatus(ClaimStatus status)
        {
            var result = status.ToString();

            if (status == ClaimStatus.Approved)
                result = "Application Complete";
            else if (status == ClaimStatus.Declined)
                result = "Deactivate";

            return result;
        }

        public static string GetHwStatusTextByClaimStatus(ClaimStatus status)
        {
            var result = status.ToString();

            if (status == ClaimStatus.Approved || status == ClaimStatus.FundingQueue)
                result = "Complete";
            else if (status == ClaimStatus.Declined)
                result = "Deactivate";

            return result;
        }

        public static string GetHwStatusByClaimStatus(ClaimStatus status)
        {
            var hwStatus = string.Empty;

            if (status == ClaimStatus.Approved)
            {
                hwStatus = "PAID";
            }
            else if (status == ClaimStatus.Declined)
            {
                hwStatus = "DECLINED";
            }
            return hwStatus;
        }
    }
}