﻿using NLog;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;

namespace ODRC.Job.Pemd.CompletedApi.Core
{
    public class CompletedApiService
    {
        private IScheduler scheduler = null;

        public CompletedApiService()
        {
        }

        public void Start()
        {
            // CronManager.Start();
            // Grab the Scheduler instance from the Factory
            StdSchedulerFactory factory = new StdSchedulerFactory();
            scheduler = factory.GetScheduler();
            scheduler.Clear();

            // define the job and tie it to our HelloJob class
            IJobDetail job = JobBuilder.Create<CompletedApiJob>()
                .WithIdentity("CompletedApi", "ApiGroup")
                .Build();

            // Trigger the job to run now, and then repeat every 10 seconds
            ICronTrigger trigger = (ICronTrigger)TriggerBuilder.Create()
                .WithIdentity("CompletedApiTrigger", "ApiGroup")
                .WithCronSchedule(ConfigurationManager.AppSettings["CronSchedule"])
                .Build();

            // Tell quartz to schedule the job using our trigger
            scheduler.ScheduleJob(job, trigger);

            // and start it off
            scheduler.Start();
            //CompletedApiJob job = new CompletedApiJob();
            //job.Execute(null);
        }

        public void Stop()
        {
            scheduler.Shutdown();
        }
    }

    /// <summary>
    /// Sends the Claim details to HW via the Completed API.
    /// 1. Read the JobPemdCompletedApiTrigger table for any ApiStatus = 0 (New)
    /// 2. Updates the JobPemdCompletedApiTrigger record to ApiStatus = 1 (In Progress)
    /// 3. Gets all the Claim details and composes the CompletedApiRequest model
    /// 4. Sends the CompletedApiRequest to HW by calling the CompletedApi
    /// 5. Records the Request and Response as well as the outcome of the CompletedApi Call for the ClaimId
    /// 6. Update the JobPemdCompletedApiTrigger record's ApiStatus = 2 (Completed)
    /// 7. Record the batch outcome
    /// 8. Send the results to the team via notification email.
    /// </summary>
    public class CompletedApiJob : IJob
    {
        private readonly string cronSchedule;
        private readonly bool sendToTest;
        private readonly StringLogger logger;

        public CompletedApiJob()
        {
            logger = new StringLogger(LogManager.GetLogger("ODRC.Job.Pemd.CompletedApi"));
            sendToTest = Convert.ToBoolean(ConfigurationManager.AppSettings["SendToTest"]);
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    connection.Open();

                    ExecuteCompletedApiRun(
                        getNewTriggers: () => CompletedApi.GetCompletedApiTriggers(connection),
                        setTriggersStatus: (fetchStatus, triggers) =>
                        {
                            var transaction = connection.BeginTransaction();
                            CompletedApi.SetTriggersStatus(fetchStatus, triggers, transaction);
                            transaction.Commit();
                        },
                        getClaimDetailsForTriggers: (triggers) =>
                        {
                            var claimDetails = new List<ClaimDetail>();

                            using (var transaction = connection.BeginTransaction())
                            {
                                claimDetails = CompletedApi.GetClaimDetails(
                                    transaction,
                                    triggers.Select(x => x.ClaimID).ToList())
                                    .ToList();
                            }

                            return claimDetails;
                        },
                        postCompletedApiRequests: (batchId, details) => CompletedApi.PostCompletedApiRequests(batchId, details, logger),
                        recordApiResults: (batchedResults) => CompletedApi.RecordApiResults(connection, batchedResults),
                        saveBatch: (batch) => CompletedApi.SaveBatch(connection, batch),
                        sendCompletedApiResults: CompletedApi.SendApiCompletionNotification);
                }
            }
            catch (Exception e)
            {
                logger.WriteErrorLog(e, "Failed executing the Completed Api Job instance.");
            }
        }

        public void ExecuteCompletedApiRun(
            Func<ICollection<JobPemdCompletedApiTrigger>> getNewTriggers,
            Action<ApiFetchStatus, ICollection<JobPemdCompletedApiTrigger>> setTriggersStatus,
            Func<ICollection<JobPemdCompletedApiTrigger>, ICollection<ClaimDetail>> getClaimDetailsForTriggers,
            Func<Guid, ICollection<ClaimDetail>, ICollection<JobPemdCompletedApiResult>> postCompletedApiRequests,
            Action<ICollection<JobPemdCompletedApiResult>> recordApiResults,
            Action<JobPemdBatch> saveBatch,
            Action<JobPemdBatch, ICollection<JobPemdCompletedApiResult>> sendCompletedApiResults)
        {
            Console.WriteLine("Execution..");
            var batch = JobPemdBatch.StartBatch();

            // 1. Read the JobPemdCompletedApiTrigger table for any ApiStatus = 0 (New)
            var newTriggers = getNewTriggers();
            batch.Size = newTriggers.Count;
            saveBatch(batch);

            if (batch.Size == 0)
            {
                // Nothing to run
                batch.Completed = DateTime.Now;
                saveBatch(batch);
                sendCompletedApiResults(batch, new List<JobPemdCompletedApiResult>());
                return;
            }

            // 2. Updates the JobPemdCompletedApiTrigger record to ApiStatus = 1 (In Progress)
            setTriggersStatus(ApiFetchStatus.InProgress, newTriggers);

            var batchedResults = new List<JobPemdCompletedApiResult>();

            foreach (var trigger in newTriggers)
            {
                var batchedTriggers = new List<JobPemdCompletedApiTrigger> { trigger };
                ICollection<JobPemdCompletedApiResult> results = null;

                try
                {


                    // 3. Gets all the Claim details and composes the CompletedApiRequest model
                    var completedApiRequests = getClaimDetailsForTriggers(batchedTriggers);

                    // 4. Sends the CompletedApiRequest to HW by calling the CompletedApi
                    results = postCompletedApiRequests(batch.Id, completedApiRequests);

                    // 5. Records the Request and Response as well as the outcome of the CompletedApi Call for the ClaimId
                    recordApiResults(results);
                }
                catch (Exception ex)
                {
                    logger.WriteErrorLog(ex, $"Failed to process trigger: { trigger.ClaimID }");
                }

                results = results == null
                    ? batchedTriggers.Select(x => new JobPemdCompletedApiResult
                    {
                        ClaimId = x.ClaimID,
                        Request = null,
                        Response = null,
                        ResponseStatusCode = null,
                        ApiResult = ApiResult.Error,
                        RunDate = DateTime.Now,
                        JobPemdBatchId = batch.Id,
                        DurationInSeconds = 0
                    }).ToList()
                    : results.ToList();
                batchedResults.AddRange(results);

                // 6. Update the JobPemdCompletedApiTrigger record's ApiStatus = 2 (Completed)
                setTriggersStatus(ApiFetchStatus.Completed, batchedTriggers);
            }

            // 7. Record batch
            batch.Completed = DateTime.Now;
            saveBatch(batch);

            // 8. Send the results to the team via notification email.
            sendCompletedApiResults(batch, batchedResults);
        }
    }
}