﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace ODRC.Job.Pemd.CompletedApi.Core
{
    public static partial class CompletedApi
    {
        public static string DateFormat = "MM/dd/yyyy";
        public static string AccountID = "FEE3NEXT";
        const string AccountProvider = "FEE3PE";
        const string UserID = "NEXT";
        public static string BaseFilePath = ConfigurationManager.AppSettings["BaseReceiptFilePath"]; // @"D:\Projects\Repo\ODRC.Web\ODRC.Web\Upload\Claims\Receipts";
        public static string ApiBaseUrl = ConfigurationManager.AppSettings["ApiBaseUrl"]; // "http://services.test.utilitywebaccess.com";
        public static string CompletedApiUrlTemplate = ConfigurationManager.AppSettings["CompletedApiUrlTemplate"];//  "/Rebates/v1/Application/{0}";
        public static bool SendToTestOnly = Convert.ToBoolean(ConfigurationManager.AppSettings["SendToTestOnly"]);
        public static string TestApiBaseUrl = ConfigurationManager.AppSettings["TestApiBaseUrl"]; // "http://services.test.utilitywebaccess.com";
        public static string TestCompletedApiUrlTemplate = ConfigurationManager.AppSettings["TestCompletedApiUrlTemplate"];//  "/Rebates/v1/Application/{0}";

        public static string ToAddressList = ConfigurationManager.AppSettings["ToAddressList"]; // "vindicator135@gmail.com,jstaszak@nxtpay.com,rguez_18@yahoo.com";
        public static string SmtpHost = ConfigurationManager.AppSettings["SmtpHost"]; // "smtp.1and1.com";
        public static string SmtpPort = ConfigurationManager.AppSettings["SmtpPort"]; // "587";
        public static string SmtpUserName = ConfigurationManager.AppSettings["SmtpUserName"]; // "test@nxtpay.com";
        public static string SmtpPassword = ConfigurationManager.AppSettings["SmtpPassword"]; // "trigger1";

        public static string[] ProgramIDs = ConfigurationManager.AppSettings["ProgramIDs"].Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries); // Program-specific

        public static List<string> TestAccountNumbers = new List<string>
        {
            "990012340001","990012340002","990012340003","990012340004","990012340005","990012340006",
            "990012340007","990012340008","990012340009","990012340010","990012340011","990012340012",
            "990012340013","990012340014","990012340015","990012340016","990012340017","990012340018",
            "990012340019","990012340020","990012340021","990012340041","990012340042","990012340043",
            "990012340044","990012340045","990012340046","990012340047","990012340048","990012340049"
        };
    }
}
