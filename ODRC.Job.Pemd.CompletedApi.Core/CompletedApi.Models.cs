﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODRC.Job.Pemd.CompletedApi.Core
{
    public enum LookupMode
    {
        StartsWith = 0,
        ExactMatch = 1,
        Contains = 2,
        ExactMatchCaseSensitive = 3
    }

    public class FieldValue
    {
        public string Field { get; set; }
        public string Value { get; set; }
    }

    public class JobPemdBatch
    {
        public Guid Id { get; set; }
        public DateTime Started { get; set; }
        public DateTime? Completed { get; set; }
        public int Size { get; set; }
        public long DurationInSeconds { get; set; }

        public static JobPemdBatch StartBatch() =>
            new JobPemdBatch
            {
                Id = Guid.NewGuid(),
                Started = DateTime.Now,
                Completed = null
            };
    }

    public class JobPemdCompletedApiResult
    {
        public int Id { get; set; }
        public Guid JobPemdBatchId { get; set; }
        public Guid ClaimId { get; set; }
        public ApiResult ApiResult { get; set; }
        public string RequestUrl { get; set; }
        public string Request { get; set; }
        public string Response { get; set; }
        public string ResponseStatusCode { get; set; }
        public decimal DurationInSeconds { get; set; }
        public DateTime RunDate { get; set; }
    }

    public enum ApiResult
    {
        Success = 0,
        Failed = 1,
        Error = 2,
        Abandoned = 3
    }

    public class ClaimDetail
    {
        public string ResourceId => this.FieldValues
                    .Where(x => x.Field.Equals("Resource ID", StringComparison.InvariantCultureIgnoreCase))
                    .Select(x => x.Value)
                    .First();

        public string UrlPath { get; set; }
        public string AlternateClaimIDValue { get; set; }
        public string AccountNumber { get; set; }
        public string AccountProvider { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string MailingAddress1 { get; set; }
        public string MailingCity { get; set; }
        public string MailingState { get; set; }
        public string MailingZipCode { get; set; }
        public string Phone { get; set; }
        public dynamic MobilePhone { get; set; }
        public string UserID { get; set; }
        public Guid ClaimID { get; set; }
        public IEnumerable<FieldValue> FieldValues { get; set; }
        public DateTime CreatedDate { get; set; }
        public ClaimStatus Status { get; set; }
        public int RewardDollarValue { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }

    public enum ApiFetchStatus
    {
        New = 0,
        InProgress = 1,
        Completed = 2
    }

    public class JobPemdCompletedApiTrigger
    {
        public int Id { get; set; }
        public Guid ClaimID { get; set; }
        public ApiFetchStatus ApiFetchStatus { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
    }

    public enum RewardTypes
    {
        DollarAmount = 1,
        TurnOffAmount,
        Points,
        ItemCount,
        Prepaid,
        Check
    }

    public enum ClaimStatus
    {
        /// <summary>
        /// Reference to jira PEMD-57 for details
        /// Invalid acct #s
        ////Non-matching acct # (from online submission)
        ////No acct # (paper submission/digitized)
        /// </summary>
        AccountVerification,
        New = 1,
        NoReceipt,
        Approved,
        Pending,
        Declined,
        ReSubmitted,
        PendingVendorValidation,
        Archived,
        Override,
        Expired,
        /// <summary>
        /// When an approved claim has been Paid
        /// </summary>
        Paid,
        /// <summary>
        /// When the Check or Prepaid or Reward has already been Sent to the customer
        /// </summary>
        SentMail,
        /// <summary>
        /// When the Check or Prepaid or Reward has been received by the customer aka money in hand
        /// </summary>
        ConfirmMail,
        /// <summary>
        /// SC 27/06 - When the Claim is Pending an API call (this came up as a required for Honeywell)
        /// </summary>
        ApiPending,
        /// <summary>
        /// SC 22/09 - Applies for PEMD claims that have been marked as duplicate (based on PEMD-29)
        /// </summary>
        DuplicateReview,
        /// <summary>
        /// wfp 20160921 - QA/QC Queue
        /// </summary>

        //Important: always add new enum entries at the bottm to avoid breaking other references.
        //Note: 
        //QAQC = 16 and ToBePaid = 17 are used in stored procedure SendExpiredQAQCToTobePaidQueue
        //hence, it's important that 16 and 17 are maintained as QAQC and ToBePaid value, respectively.
        QualityControl = 16,
        ToBePaid = 17,
        /// <summary>
        /// sc 20160928 - Funding Queue
        /// This is used to hold all Approved and To Be Paid claims in one view for a custom Funding Queue page.
        /// Specific to PEMD client
        /// </summary>
        FundingQueue = 18,
        /// <summary>
        /// sc 20160929 - For PEMD, when the Pay checkbox has been ticked from the Funding Queue
        /// </summary>
        Complete,
        /// <summary>
        /// sc 20160929 - For PEMD, when the Hold checkbox has been ticked from the Funding Queue
        /// </summary>
        Hold,
        QualityAssurance = 21
    }
}
