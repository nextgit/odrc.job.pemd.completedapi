﻿using Topshelf;
using System;
using ODRC.Job.Pemd.CompletedApi.Core;
using System.Configuration;

namespace ODRC.Job.Pemd.CompletedApi.Host
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceName = ConfigurationManager.AppSettings["ServiceName"];
            HostFactory.Run(x =>
            {
                x.Service<CompletedApiService>(s =>
                {
                    s.ConstructUsing(name => new CompletedApiService());
                    s.WhenStarted(ss => ss.Start());
                    s.WhenStopped(ss => ss.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("Sends the Claim details to Honeywell via the Complete API specification.");
                x.SetDisplayName(serviceName);
                x.SetServiceName(serviceName);
            });
        }
    }
}
